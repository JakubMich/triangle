def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    sorted_sides_triangle = sorted([a,b,c])
    if sorted_sides_triangle[0] + sorted_sides_triangle[1] > sorted_sides_triangle[2]:
        return True
    return False
